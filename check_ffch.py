#!/usr/bin/python3
# SPDX-FileCopyrightText: 2021 Helmut Grohne <helmut@subdivi.de>
# SPDX-License-Identifier: MIT

"""Pass a yaml file as only argument to this script. The yaml file should
conatin a mapping of (arbitrary) node names to objects mapping "mac" to the mac
address of the node and "links". The latter maps local interfaces to a list of
node names. The script verifies that the node has a link to the target nodes on
the relevant interfaces and complains otherwise.

Example of two nodes seeing each other on mesh0:

node1:
    mac: 11:22:33:44:55:66
    links:
      mesh0:
        - node2
node2: 
    mac: 22:33:44:55:66:77
    links:
      mesh0:
        - node1
"""

import asyncio
import html.parser
import json
import sys

import aiohttp
import yaml

def mac2ip(mac: str) -> str:
    parts = [int(part, 16) for part in mac.split(":")]
    parts[0] ^= 2
    return "2001:bc8:3f13:ffc2:%x%02x:%xff:fe%02x:%x%02x" % tuple(parts)


class StatusParser(html.parser.HTMLParser):
    def __init__(self):
        super().__init__()
        self.interfaces = {}

    def handle_starttag(self, tag, attrs):
        if tag != "div":
            return
        attrdict = dict(attrs)
        try:
            self.interfaces[attrdict["data-interface"]] = \
                    attrdict["data-interface-address"]
        except KeyError:
            pass


async def fetch_info(mac: str):
    result = {}
    urlroot = "http://[%s]/cgi-bin/" % mac2ip(mac)
    async with aiohttp.ClientSession(raise_for_status=True) as session:
        async with session.get(urlroot + "status") as resp:
            parser = StatusParser()
            parser.feed(await resp.text())
            result["interfaces"] = parser.interfaces
        async with session.get(urlroot + "dyn/neighbours-batadv") as resp:
            data = await resp.content.readline()
            if data.startswith(b"data: "):
                data = data[6:]
            result["links"] = json.loads(data)
    return result


async def main():
    with open(sys.argv[1], "rb") as f:
        nodes = yaml.safe_load(f)

    tasks = {}
    for name, obj in nodes.items():
        tasks[name] = asyncio.create_task(fetch_info(obj["mac"]))

    info = {}
    for name, task in tasks.items():
        try:
            info[name] = await task
        except Exception as e:
            print(f"caught exception while fetching info for {name}: {e}")

    for name, config in nodes.items():
        if name not in info:
            print(f"node {name} is not reachable")
            continue
        for iface, targets in config["links"].items():
            for target in targets:
                if target not in nodes:
                    print(f"unknown {target} referenced from {name}")
                    continue
                if target not in info:
                    continue  # complained earlier
                for targetmac, targetobj in info[name]["links"].items():
                    if targetobj["ifname"] != iface:
                        continue
                    if targetmac in info[target]["interfaces"].values():
                        break
                else:
                    print(f"missing {target} from {name} {iface}")

if __name__ == "__main__":
    asyncio.run(main())
