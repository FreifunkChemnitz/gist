#!/usr/bin/python3
# SPDX-FileCopyrightText: 2021 Helmut Grohne <helmut@subdivi.de>
# SPDX-License-Identifier: MIT

import subprocess

import pyparsing
import jinja2

Number = pyparsing.pyparsing_common.number
String = pyparsing.QuotedString("'")
Boolean = pyparsing.Keyword('true').setParseAction(lambda: True) | \
          pyparsing.Keyword('false').setParseAction(lambda: False)
Value = pyparsing.Forward()
List = pyparsing.Suppress('{') + \
       pyparsing.delimitedList(Value, delim=',') \
           .setParseAction(lambda ts: [ts.asList()]) + \
       (pyparsing.Optional(',') + '}').suppress()
Identifier = pyparsing.Word(pyparsing.alphanums + "_")
Assignment = pyparsing.Group(Identifier + pyparsing.Suppress('=') + Value)
Object = pyparsing.Suppress('{') + \
         pyparsing.delimitedList(Assignment, delim=',') \
             .setParseAction(lambda ts: dict(ts.asList())) + \
         (pyparsing.Optional(',') + '}').suppress()
Value <<= Object | List | Number | String | Boolean
Object.ignore('--' + pyparsing.restOfLine)

fastd_conf_template = jinja2.Template("""interface "{{ interface }}";
secret "{{ secret }}";
forward no;
mtu {{ site_conf.mesh_vpn.mtu }};
user "{{ user }}";
group "{{ group }}";
{%- for groupname, group in domain_conf.mesh_vpn.fastd.groups.items() %}
peer group "{{ groupname }}" {
	peer limit {{ group.limit }};
{%- for peername, peer in group.peers.items() %}
	peer "{{ peername }}" {
		key "{{ peer.key }}";
{%- for remote in peer.remotes %}
		remote {{ remote }};
{%- endfor %}
	}
{%- endfor %}
}
{%- endfor %}
""")

def main():
    site_conf = Object.parseFile("site.conf").asList()[0]
    domain_conf = Object.parseFile("domains/chemnitz.conf").asList()[0]
    secret = subprocess.check_output(
        ["fastd", "--generate-key", "--machine-readable"],
                                     encoding="ascii").strip()
    print(fastd_conf_template.render(
        interface="ffch-mesh-vpn",
        secret=secret,
        user="fastd-ffch",
        group="fastd-ffch",
        site_conf=site_conf,
        domain_conf=domain_conf
    ))

if __name__ == "__main__":
    main()
